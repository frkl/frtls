# -*- coding: utf-8 -*-
import logging
import sys
import textwrap

import click

from colorama import Fore, Style
from frtls.cli.terminal import get_terminal_size, output_to_terminal
from frtls.exceptions import FrklException, ensure_frkl_exception
from frtls.strings import reindent


log = logging.getLogger("frtls")


def pretty_print_exception(exc: Exception):
    """Pretty prints an exception to the terminal."""

    frkl_exc: FrklException = ensure_frkl_exception(exc)

    cols, _ = get_terminal_size()

    msg = Fore.RED + "Error: " + Style.RESET_ALL + frkl_exc.msg
    for m in msg.split("\n"):
        m = textwrap.fill(m, width=cols, subsequent_indent="       ")
        output_to_terminal(m)
    click.echo()
    if frkl_exc.reason:
        output_to_terminal(Style.BRIGHT + "  Reason:" + Style.RESET_ALL)
        msg = reindent(frkl_exc.reason, 4)

        for m in msg.split("\n"):
            m = textwrap.fill(m, width=cols, subsequent_indent="    ")
            output_to_terminal(m)
        click.echo()
    if frkl_exc.parent is not None:
        output_to_terminal(Style.BRIGHT + "  Parent exception:" + Style.RESET_ALL)
        msg = reindent(repr(frkl_exc.parent), 4)
        output_to_terminal(msg)
        click.echo()
    if frkl_exc.solution:
        output_to_terminal(Style.BRIGHT + "  Solution: " + Style.RESET_ALL)
        msg = reindent(frkl_exc.solution, 4)
        for m in msg.split("\n"):
            m = textwrap.fill(m, width=cols, subsequent_indent="    ")
            output_to_terminal(m)
        click.echo()
    if frkl_exc.references:
        if len(frkl_exc.references) == 1:
            url = frkl_exc.references[list(frkl_exc.references.keys())[0]]
            output_to_terminal(Style.BRIGHT + "  Reference: " + Style.RESET_ALL + url)
        else:
            output_to_terminal(Style.BRIGHT + "  References:" + Style.RESET_ALL)
            for k, v in frkl_exc.references.items():
                output_to_terminal("    " + Style.DIM + k + ": " + Style.RESET_ALL + v)

    click.echo()


def handle_exc(func, exit=True, exit_code=1):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (Exception) as e:
            handle_exception(e, exit=exit, exit_code=exit_code)

    return func_wrapper


def handle_exc_async(func, exit=True, exit_code=1):
    async def func_wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except (Exception) as e:

            handle_exception(e, exit=exit, exit_code=exit_code)

    func_wrapper.__name__ = func.__name__
    func_wrapper.__doc__ = func.__doc__
    return func_wrapper


def handle_exception(
    exc: Exception, exit: bool = True, exit_code: int = 1, logger=None
):

    if logger is None:
        logger = log

    log.debug(exc, exc_info=True)
    # click.echo("Can't create context: {}".format(e))

    if hasattr(exc, "root_exc"):

        root_exc = exc.root_exc  # type: ignore
        if isinstance(root_exc, FrklException):
            exc = root_exc
        else:
            exc = FrklException(parent=exc)
    if not isinstance(exc, FrklException) and not issubclass(
        exc.__class__, FrklException
    ):
        exc = FrklException(exc)

    print()
    pretty_print_exception(exc)

    if exit:
        sys.exit(exit_code)
