# -*- coding: utf-8 -*-
import logging
import sys
from typing import Any, Callable, Mapping, Optional

import asyncclick as click
from frtls.args.hive import ArgHive
from frtls.async_helpers import _run_in_thread
from frtls.cli.exceptions import handle_exception
from frtls.types.utils import is_instance_or_subclass


log = logging.getLogger("frtls")


class FrklBaseCommand(click.MultiCommand):

    # __metaclass__ = ABCMeta

    def __init__(
        self,
        name: str = None,
        # print_version_callback=None,
        invoke_without_command: bool = False,
        no_args_is_help=None,
        chain: bool = False,
        result_callback: Callable = None,
        arg_hive: ArgHive = None,
        params=None,
        # help=None,
        callback=None,
        **kwargs
    ):

        # self.print_version_callback = print_version_callback
        self._group_params: Optional[Mapping[str, Any]] = None
        self._group_params_parsed: Optional[Mapping[str, Any]] = None

        if arg_hive is None:
            arg_hive = ArgHive()

        self._arg_hive = arg_hive

        if hasattr(self, "get_group_options"):
            args = self.get_group_options()
        else:
            args = {}
        self._group_parameter_args = arg_hive.create_record_arg(args)
        self._arg_renderer = self._group_parameter_args.create_arg_renderer(
            "cli", add_defaults=True
        )

        params_args = None
        if args:
            params_args = self._arg_renderer.rendered_arg

        if params:
            p = params
        else:
            p = []

        if params_args:
            p.extend(params_args)

        super(FrklBaseCommand, self).__init__(
            name=name,
            invoke_without_command=invoke_without_command,
            no_args_is_help=no_args_is_help,
            chain=chain,
            result_callback=result_callback,
            params=p,
            callback=callback,
            **kwargs
        )

        self._initialized = False

    def _init_command(self, ctx):

        if self._initialized:
            return

        self._group_params = ctx.params

        av = self._arg_renderer.create_arg_value(self._group_params)
        self._group_params_parsed = av.processed_input

        if ctx.obj is None:
            ctx.obj = {}
        self._initialized = True
        self.init_command(ctx)

    def init_command(self, ctx):
        pass

    # async def _init_command_async(self, ctx):
    #
    #     await self._init_command_async(ctx)

    async def init_command_async(self, ctx):
        pass

    def list_commands(self, ctx):

        try:

            self._init_command(ctx)

            async def wrap(c):
                await self.init_command_async(c)
                return await self._list_commands(c)

            result = _run_in_thread(wrap, ctx)
            if is_instance_or_subclass(result, Exception):
                raise result

            return result
        except (Exception) as e:
            handle_exception(e)

    def get_command(self, ctx, name):
        try:
            self._init_command(ctx)

            async def wrap(c, n):
                await self.init_command_async(c)
                return await self._get_command(c, n)

            result = _run_in_thread(wrap, ctx, name)

            if is_instance_or_subclass(result, Exception):
                raise result
            return result
        except (Exception) as e:
            handle_exception(e)
            sys.exit(2)
