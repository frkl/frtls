# -*- coding: utf-8 -*-
import copy
import logging
import typing
from abc import ABCMeta, abstractmethod
from collections.abc import Mapping
from typing import Any, Dict, Iterable, List, MutableMapping, Optional

from frtls.args.renderers import ArgRenderer
from frtls.dicts import get_seeded_dict
from frtls.doc.doc import Doc
from frtls.exceptions import ArgValidationError, FrklException
from frtls.types.utils import is_instance_or_subclass


log = logging.getLogger("frtls")


DEFAULT_ARG_DICT = {
    "required": True,
    "empty": False,
    "multiple": False,
    "default": None,
}

SCALAR_MAP = {
    "any": {"python": Any},
    "null": {"python": None},
    "boolean": {"python": bool},
    "int": {"python": int},
    "long": {"python": int},
    "float": {"python": float},
    "double": {"python": float},
    "bytes": {"python": bytes},
    "string": {"python": str},
    "dict": {"python": Mapping},
    "list": {"python": List},
    "enum": {"python": str},
    "fixed": {"python": bytes},
}


class Arg(metaclass=ABCMeta):

    _plugin_type = "instance"

    def __init__(self, id: str, **properties):

        self._id = id
        self._properties = get_seeded_dict(self._get_seed_dict(), properties)

    @abstractmethod
    def _get_seed_dict(self) -> MutableMapping:
        pass

    @abstractmethod
    def _validate(self, value: Any) -> Any:
        pass

    @property
    def id(self):
        return self._id

    @property
    def properties(self):
        return self._properties

    @property
    def required(self):
        return self._properties["required"]

    @property
    def multiple(self):
        return self._properties["multiple"]

    @property
    def empty(self):
        return self._properties["empty"]

    @property
    def default(self):
        default = self._properties["default"]

        if callable(default):
            result = default(self)
        else:
            result = default
        return result

    @property
    def doc(self) -> Doc:
        return Doc(self._properties.get("doc", None))

    def validate(self, value: Any, raise_exception: bool = False) -> Any:

        validated = self._validate(value)
        if raise_exception and is_instance_or_subclass(validated, ArgValidationError):
            raise validated

        return validated

    def to_dict(self):

        return self._properties

    def create_arg_renderer(self, renderer_type: str, **renderer_config):

        if renderer_type != "cli":
            raise NotImplementedError()
        from frtls.args.renderers.cli import CliArgRenderer

        renderer = CliArgRenderer(arg_name=self.id, arg=self, **renderer_config)
        return renderer

    @property
    def scalar_type(self):

        return find_scalar_type(self)

    def __repr__(self):

        return f"[{self.__class__.__name__}: id={self.id} properties={self._properties}"


class ScalarArg(Arg):

    _plugin_name = "scalar"

    def __init__(self, id: str, **properties):

        super().__init__(id=id, **properties)

    def _get_seed_dict(self) -> MutableMapping:

        return DEFAULT_ARG_DICT

    def _validate(self, value: Any) -> Any:

        if self.required and value is None:
            value = ArgValidationError(
                self, value, msg="No value provided for required field."
            )
        elif value is None and self.default:
            value = copy.deepcopy(self.default)

        return value


class DerivedArg(Arg):

    _plugin_name = "derived"

    def __init__(self, id: str, parent_type: Arg, **properties):

        self._parent_type = parent_type

        super().__init__(id=id, **properties)

    def _get_seed_dict(self) -> MutableMapping:

        return self._parent_type.to_dict()

    def _pre_check_value(self, value: Any) -> Any:
        """Hook to pre-check/convert value before parent validation."""

        return value

    def _validate(self, value: Any) -> Any:

        if value is None and self.default:
            value = copy.deepcopy(self.default)
        elif value is None and self._parent_type.default:
            value = copy.deepcopy(self._parent_type.default)
        elif self.required and value is None:
            value = ArgValidationError(
                self,
                value,
                msg="No value provided for required field and no default set.",
            )

        value = self._pre_check_value(value)
        if value is not None:
            value = self._parent_type.validate(value, raise_exception=False)
        return value

    def __repr__(self):

        return f"[{self.__class__.__name__}: id={self.id} parent_type={self._parent_type} properties={self._properties}"


class RecordArg(Arg):

    _plugin_name = "record"

    def __init__(self, id: str, childs: Dict[str, Arg], **properties):

        self._childs: Dict[str, Arg] = childs

        self._arg_renderers: Mapping[str, ArgRenderer] = {}
        super().__init__(id=id, **properties)

    @property
    def childs(self) -> typing.Mapping[str, Arg]:

        return self._childs

    def to_dict(self) -> typing.Mapping[str, Any]:

        result = dict(self._properties)
        result["childs"] = {}
        for k, v in self.childs.items():
            result[k] = v.to_dict()
        return result

    def get_child_arg(self, name: str) -> Optional[Arg]:

        return self._childs.get(name, None)

    def _validate(self, value: Any) -> Any:

        # TODO: use defaults
        result = {}
        errors: Dict[str, ArgValidationError] = {}
        for field_name, arg in self._childs.items():

            v = value.get(field_name, self.default.get(field_name, None))
            validated = arg.validate(v)

            if is_instance_or_subclass(validated, ArgValidationError):
                errors[field_name] = validated
            else:
                result[field_name] = validated

        if errors:
            return ArgValidationError(self, value, child_errors=errors)
        else:
            return result

    @property
    def arg_names(self) -> Iterable[str]:

        return self._childs.keys()

    # def get_defaults(self) -> typing.Mapping[str, Any]:
    #
    #     return self.properties["default"]
    #     # result = {}
    #     # for name, arg in self._childs.items():
    #     #     if arg.default is not None:
    #     #         result[name] = arg.default
    #
    #     return result

    def _get_seed_dict(self) -> MutableMapping:

        return DEFAULT_ARG_DICT

    # def from_cli_input(
    #     self, _remove_none_values: bool = True, **user_input: Any
    # ) -> typing.Mapping[str, Any]:
    #
    #     renderer = self.get_arg_renderer("cli")
    #
    #     renderer.input = user_input
    #
    #     result = {}
    #     for param in self.to_cli_options():
    #         if param.name in user_input.keys():
    #             value = from_cli_option(user_input[param.name], param)
    #             result[param.name] = value
    #
    #     if _remove_none_values:
    #         temp = {}
    #         for k, v in result.items():
    #             if v is not None:
    #                 temp[k] = v
    #         result = temp
    #
    #     return result

    # def to_cli_options(
    #     self
    # ) -> Iterable[Parameter]:
    #
    #     renderer = self.create_arg_renderer("cli")
    #     return renderer.rendered_arg

    def __repr__(self):

        return f"[{self.__class__.__name__}: id={self.id} childs={list(self._childs.keys())} properties={self._properties}"


def find_scalar_type(arg: Arg, orig_arg: Optional[Arg] = None) -> str:

    if orig_arg is None:
        orig_arg = arg

    if isinstance(arg, ScalarArg):
        arg_type = arg.id
    else:
        if not hasattr(arg, "_parent_type"):
            raise FrklException(
                msg=f"Can't process argument '{orig_arg.id}'.",
                reason="Can't find scalar parent arg.",
            )
        parent = arg._parent_type  # type: ignore
        return find_scalar_type(parent, orig_arg=orig_arg)

    return arg_type
