# -*- coding: utf-8 -*-
import importlib
import inspect
import logging
import pkgutil
import uuid
from typing import Any, Dict, List, Sequence, Type, Union

from frtls.defaults import DEFAULT_KEY_FORMATS
from frtls.exceptions import FrklException
from frtls.strings import from_camel_case


log = logging.getLogger("frtls")


def get_all_subclasses(cls: Type) -> List[Type]:
    """Find all (loaded) subclasses of a given base-class.

    Args:

        - cls: the base class

    Returns:

        - a list of all subclasses
    """
    all_subclasses = []

    # import pp
    # print(cls)
    # pp(type(cls))
    # pp(cls.__dict__)

    for subclass in cls.__subclasses__():
        all_subclasses.append(subclass)
        all_subclasses.extend(get_all_subclasses(subclass))

    return all_subclasses


def get_class_from_string(class_name: str) -> Type:
    """Return the class type object of the specified class name.

    Args:

        - *class_name*: the class path string

    Returns:

        - the Type object of the requested class
    """

    if "." in class_name:
        module_name, _, class_name = class_name.rpartition(".")
        return get_class(module_name, class_name)
    else:
        raise ValueError(
            f"No module name provided when trying to get class '{class_name}'"
        )


def get_class(module_name: str, class_name: str) -> Type:
    """Return a class type object of the specified module and class names.

    Raises an `ImportError` if the module can't be loaded, and an `AttributeError` if the class can't be found.

    Args:

        - *module_name*

    Returns:
        - the Type object of the requested class
    """

    if not module_name:
        raise ValueError(
            f"No module name provided when trying to get class '{class_name}'"
        )
    m = importlib.import_module(module_name)
    c = getattr(m, class_name)

    return c


def is_instance_or_subclass(obj: Any, base_class: Type) -> bool:

    return isinstance(obj, base_class) or issubclass(obj.__class__, base_class)


def _generate_valid_identifier_seq(text, sep=None, allow_leading_underscore=False):

    itr = iter(text)
    # pull characters until we get a legal one for first in identifer
    for ch in itr:
        if (allow_leading_underscore and ch == "_") or ch.isalpha():
            yield ch
            break
    # pull remaining characters and yield legal ones for identifier
    for ch in itr:
        if ch == "_" or ch.isalpha() or ch.isdigit():
            yield ch
        elif sep:
            yield sep


def generate_valid_identifier(
    name=None,
    first_character_uppercase=False,
    prefix=None,
    length_without_prefix=-1,
    sep=None,
    allow_leading_underscore=False,
):

    if name is None:
        name = str(uuid.uuid4())
    elif not isinstance(name, str):
        name = str(name)

    if prefix:
        name = f"{prefix}{name}"

    id = "".join(
        _generate_valid_identifier_seq(
            name, sep=sep, allow_leading_underscore=allow_leading_underscore
        )
    )
    if first_character_uppercase:
        id = id.capitalize()

    if length_without_prefix > 0:
        if prefix:
            id = id[0 : length_without_prefix + len(prefix)]  # noqa
        else:
            id = id[0:length_without_prefix]

    return id


def get_type_alias(
    ty: Union[Type, str], include_module_path=True, alias_type="underscore"
) -> str:

    if alias_type != "underscore":
        raise NotImplementedError()

    if isinstance(ty, type):
        type_name = ty.__name__
    else:
        type_name = ty.split(".")[-1]

    alias = from_camel_case(type_name, sep="_")

    if include_module_path:
        if isinstance(ty, str):
            if "." in ty:
                module_path = ty.split(".")[0:-1]
                module_path.append(alias)
                alias = ".".join(module_path)
        else:
            alias = f"{ty.__class__.__module__}.{alias}"

    return alias


def create_type_alias_map(
    type_obj: Type, key_formats: List[str], remove_postfixes: List[str] = None
):

    result = {}
    type_name = type_obj.__name__

    aliases = set()
    if "class_name" in key_formats:
        aliases.add(type_name)
    if "dash" in key_formats:
        tsc_id_string = from_camel_case(type_name, sep="-")
        aliases.add(tsc_id_string)
    if "underscore" in key_formats:
        tsc_id_string = from_camel_case(type_name, sep="_")
        tsc_id_string_dash = tsc_id_string.replace("-", "_")
        aliases.add(tsc_id_string_dash)

    for alias in aliases:

        key = alias
        if remove_postfixes:
            if isinstance(remove_postfixes, str):
                remove_postfixes = [remove_postfixes]

            for pf in remove_postfixes:
                try:
                    if alias.lower().endswith(pf.lower()):
                        temp = alias[0 : -len(pf)]  # noqa
                        if temp:

                            if temp[-1] in ["_", "-"]:
                                temp = temp[0:-1]
                                if temp:
                                    key = temp
                            else:
                                key = temp

                except (Exception) as e:
                    log.debug(f"Could not remove postfix: {e}", exc_info=True)

        result[key] = type_obj

    return result


def add_type_aliases(
    type_map: Dict[str, Type],
    type_obj: Type,
    key_formats: List[str] = DEFAULT_KEY_FORMATS,
    remove_postfixes: List[str] = None,
):

    aliases = create_type_alias_map(
        type_obj=type_obj, key_formats=key_formats, remove_postfixes=remove_postfixes
    )
    # TODO: check for duplicates
    type_map.update(aliases)

    return aliases.keys()


def load_modules(*modules: Union[None, str, Sequence[str]]) -> List:

    result: List = []
    if not modules:
        return result

    for mod in modules:
        if not mod:
            continue
        if isinstance(mod, str):
            if "*" in mod:
                if not mod.endswith(".*"):
                    raise FrklException(
                        msg=f"Can't import module string '{mod}'.",
                        reason="String must either not contain '*', or end with '.*'.",
                    )

                base = ".".join(mod.split(".")[0:-1])
                base_mod = importlib.import_module(base)
                result.append(base_mod)

                # prefix = base_mod.__name__ + "."
                prefix = base + "."
                for p in pkgutil.iter_modules(
                    base_mod.__path__, prefix  # type: ignore
                ):
                    m = __import__(p[1], fromlist="dummy")
                    result.append(m)

                # special handling when the package is bundled with PyInstaller 3.5
                # See https://github.com/pyinstaller/pyinstaller/issues/1905#issuecomment-445787510
                toc: Any = set()
                for importer in pkgutil.iter_importers(
                    base_mod.__name__.partition(".")[0]
                ):
                    if hasattr(importer, "toc"):
                        toc |= importer.toc
                for name in toc:
                    if name.startswith(prefix):
                        m = __import__(name, fromlist="dummy")
                        result.append(m)

            else:
                m = importlib.import_module(mod)
                result.append(m)
        elif isinstance(mod, Sequence):
            ms = load_modules(*mod)
            result.extend(ms)
        else:
            raise TypeError(f"Invalid module type: {type(mod)}")

    return result


def find_sub_classes(
    base_class: Type,
    preload_modules: Union[str, Sequence[str]] = None,
    key_formats: List[str] = DEFAULT_KEY_FORMATS,
    remove_postfixes: List[str] = None,
) -> Dict[str, Type]:

    load_modules(preload_modules)

    type_subclasses = get_all_subclasses(base_class)
    result: Dict[str, Type] = {}

    for tsc in type_subclasses:
        if inspect.isabstract(tsc):
            log.debug(f"not using subclass '{tsc}': class is abstract")
            continue
        add_type_aliases(
            result,
            type_obj=tsc,
            key_formats=key_formats,
            remove_postfixes=remove_postfixes,
        )

    return result
