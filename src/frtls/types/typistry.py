# -*- coding: utf-8 -*-

import inspect
import logging
from fnmatch import fnmatch
from typing import Dict, Iterable, List, Optional, Type, Union

from frtls.exceptions import FrklException
from frtls.types.plugins import TypistryPluginManager
from frtls.types.utils import (
    get_all_subclasses,
    get_class_from_string,
    get_type_alias,
    load_modules,
)


log = logging.getLogger("frtls")


def ensure_type(cls: Union[str, Type]) -> Type:

    if isinstance(cls, type):
        return cls

    else:
        return get_class_from_string(cls)


class TypistryException(FrklException):
    def __init__(self, *args, **kwargs):

        self._typistry = kwargs.pop("typistry")
        super().__init__(*args, **kwargs)


class Typistry(object):
    def __init__(self, *args, **kwargs) -> None:

        self._modules: List[str] = []
        self._classes: List[Type] = []
        self._registered: Dict[Type, Dict] = {}
        self._invalidated: bool = False
        self._aliases: Dict[str, Type] = {}
        self._ids: Dict[Type, str] = {}
        self._id_strategy: str = "alias"

        modules: List[str] = kwargs.get("modules", None)
        if modules:
            self.add_module_paths(*modules)

        classes = kwargs.get("classes", None)
        if classes:
            self.add_classes(*classes)

    def add_classes(self, *classes):

        if not classes:
            return

        to_add = []
        for cls in classes:
            c = ensure_type(cls)
            to_add.append(c)
        self.add_module_paths(*[cls.__module__ for cls in to_add])
        self.register_classes(*to_add)

    def _invalidate(self) -> None:

        self._invalidated = True  # type: ignore

    @property
    def invalidated(self) -> bool:
        return self._invalidated  # type: ignore

    def add_module_paths(self, *module_paths: str) -> None:

        if not module_paths:
            return

        load_modules(module_paths)
        changed = False
        for module in module_paths:
            if module not in self.modules:
                changed = True
                self.modules.append(module)

        if changed:
            self._invalidate()

    @property
    def modules(self) -> List[str]:
        return self._modules  # type: ignore

    @property
    def base_classes(self) -> Iterable[Type]:
        return self.registered_classes.keys()

    @property
    def aliases(self) -> Dict[str, Type]:

        return self._aliases

    def get_id(self, cls: Type) -> Optional[str]:

        return self._ids.get(cls, None)

    @property
    def registered_classes(self) -> Dict[Type, Dict]:

        return self._registered  # type: ignore

    def check_allowed(self, cls: Union[Type, str], raise_exception=True) -> bool:
        """Check whether the specified class is allowed in this context.

        Currently, this means whether it is under any of the modules in the 'modules' list attribute.

        Args:
            - *cls*: the type to check
        """

        if isinstance(cls, str) and cls in self.aliases.keys():
            _cls = self.aliases[cls]
        else:
            _cls = ensure_type(cls)
        path = _cls.__module__

        for m in self.modules:
            if fnmatch(path, m):
                return True

            if m.endswith(".*"):
                if m[0:-2] == path:
                    return True

        if raise_exception:
            if not self.modules:
                raise TypistryException(
                    typistry=self,
                    msg=f"Type '{cls}' not allowed as member of this Typistry.",
                    reason="No python modules paths added yet to this Typestry.",
                )
            else:
                raise TypistryException(
                    typistry=self,
                    msg=f"Type '{cls}' not allowed as member of this Typistry.",
                    reason=f"Not part of any of the allowed modules: {', '.join(self.modules)}",
                )
        return False

    def _add_to_aliases(self, cls: Type) -> None:

        cls_name = cls.__name__
        cls_name_full = f"{cls.__module__}.{cls.__name__}"
        cls_alias = get_type_alias(cls)
        cls_alias_name = cls_alias.split(".")[-1]

        if cls_name in self.aliases.keys() or cls_alias_name in self.aliases.keys():
            return
            # raise TypistryException(
            #     typistry=self,
            #     msg=f"Can't add type '{cls}'.",
            #     reason=f"Already in alias map: {', '.join(self.aliases.keys())}",
            # )

        self.aliases[cls_name] = cls
        self.aliases[cls_name_full] = cls
        self.aliases[cls_alias] = cls
        self.aliases[cls_alias_name] = cls

        if self._id_strategy == "alias":
            self._ids[cls] = cls_alias_name

    def is_registered(self, cls: Union[str, Type]) -> bool:

        if isinstance(cls, str):
            return cls in self.aliases.keys()
        else:
            return cls in self.registered_classes.keys()

    def get_class(self, cls: Union[str, Type]) -> Type:

        if isinstance(cls, str):
            val = self.aliases.get(cls, None)
            if val is not None:
                return val

            _cls = ensure_type(cls)
            self.register_classes(_cls)
            return _cls
        else:
            self.register_classes(cls)
            return cls

    def register_classes(self, *classes: Union[str, Type]) -> None:

        for _cls in classes:
            if isinstance(_cls, str):
                cls = ensure_type(_cls)
            else:
                cls = _cls
                self.add_module_paths(cls.__module__)

            registered = self.registered_classes.get(cls, None)
            if registered is None:
                self.check_allowed(cls)
                registered = self.registered_classes.setdefault(cls, {})
                self._add_to_aliases(cls)

    def get_subclasses(self, base_class: Union[Type, str]) -> List[Type]:
        """Return all (non-abstract) subclasses of a base class.

        The base class must pass the 'check_allowed' check.
        """

        if not self.is_registered(base_class):
            self.register_classes(base_class)

        _base_class = self.get_class(base_class)
        cls_dict = self.registered_classes[_base_class]

        if "subclasses" in cls_dict.keys():
            return cls_dict["subclasses"]

        # subclasses_list = cls_dict.setdefault("subclasses", [])
        # if not inspect.isabstract(_base_class):
        #     subclasses_list.append(_base_class)

        all_subclasses = get_all_subclasses(_base_class)

        self.registered_classes[_base_class].setdefault("subclasses", [])
        for sc in all_subclasses:
            if inspect.isabstract(sc):

                log.debug(f"Ignoring subclass '{sc}': is abstract")
                continue
            log.debug(f"Adding subclass: {sc}")
            if sc in self.registered_classes[_base_class]["subclasses"]:
                continue

            self._add_to_aliases(sc)
            self.registered_classes[_base_class]["subclasses"].append(sc)

        return self.registered_classes[_base_class]["subclasses"]

    def get_plugin_manager(
        self,
        base_class: Union[Type, str],
        manager_name: Optional[str] = None,
        **manager_config,
    ) -> TypistryPluginManager:

        if not self.is_registered(base_class):
            self.register_classes(base_class)

        _base_class = self.get_class(base_class)
        cls_dict = self.registered_classes[_base_class]

        if manager_name is None:
            _manager_name: str = self.get_id(_base_class)  # type: ignore
        else:
            _manager_name = manager_name

        plugin_managers = cls_dict.setdefault("plugin_managers", {})

        if _manager_name in plugin_managers.keys():
            if manager_config:
                log.info(
                    f"plugin manager for '{base_class}' exists, but config provided. This could be an issue..."
                )
            return plugin_managers[_manager_name]

        subclasses = self.get_subclasses(base_class=_base_class)

        plugin_manager = TypistryPluginManager(
            _base_class, *subclasses, manager_name=_manager_name, **manager_config
        )

        cls_dict["plugin_managers"][_manager_name] = plugin_manager
        return plugin_manager
