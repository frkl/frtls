# -*- coding: utf-8 -*-
import socket


def find_free_port(start):

    in_use = True
    while in_use:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            in_use = s.connect_ex(("localhost", start)) == 0
            if in_use:
                start = start + 1
            else:
                return start
