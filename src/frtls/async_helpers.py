# -*- coding: utf-8 -*-
import asyncio
import logging
from threading import Thread
from typing import Any, Callable, Dict

from frtls.types.utils import is_instance_or_subclass


log = logging.getLogger("frtls")


def wrap_async_task(
    coroutine: Callable, *args: Any, _raise_exception: bool = True, **kwargs: Any
) -> Any:
    async def wrap():
        return await coroutine(*args, **kwargs)

    # task = asyncio.create_task(wrap())
    result = _run_in_thread(wrap, _raise_exception=_raise_exception)
    return result


def _run_in_thread(func: Callable, *args, _raise_exception=True):

    result: Dict[str, Any] = {}

    def wrap(result_holder):

        try:
            loop = asyncio.new_event_loop()
            result_holder["result"] = loop.run_until_complete(func(*args))
        except (Exception) as e:
            log.debug(f"Error in thread: {e}", exc_info=True)
            result_holder["result"] = e

    t = Thread(target=wrap, args=(result,))
    t.start()
    t.join()

    if _raise_exception:
        if is_instance_or_subclass(result["result"], Exception):
            raise result["result"]

    return result["result"]
