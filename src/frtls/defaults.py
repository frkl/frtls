# -*- coding: utf-8 -*-
import os
import sys
from pathlib import Path


if not hasattr(sys, "frozen"):
    FRTLS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frtls` module."""
else:
    FRTLS_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "frtls")  # type: ignore
    """Marker to indicate the base folder for the `frtls` module."""
FRTLS_RESOURCES_FOLDER = os.path.join(FRTLS_MODULE_BASE_FOLDER, "resources")

DEFAULT_LOG_NAMES = (
    "tings",
    "plugtings",
    "freckles",
    "ting",
    "frtls",
    "frkl",
    "nsbl",
    "tempting",
    "shellting",
    "bring",
    "freckworks",
    "retailiate",
    "retailiate-client",
)
"""Default logger names to be used for batch-setting log-levels on application entry."""

URL_PLACEHOLDER = -9876
"""Unique placeholder object used in url abbreviations."""

DEFAULT_URL_ABBREVIATIONS_FILE = {
    "gh": "https://raw.githubusercontent.com/{user}/{repo}/{branch}/{path}",
    "bb": "https://bitbucket.org/{user}/{repo}/src/{branch}/{path}",
    "gl": "https://gitlab.com/{user}/{repo}/raw/{branch}/{path}",
}
"""Built in url abbreviations for remote files."""

DEFAULT_URL_ABBREVIATIONS_GIT_REPO = {
    "gh": "https://github.com/{user}/{repo}.git",
    "bb": "https://bitbucket.org/{user}/{repo}.git",
    "gl": "https://gitlab.com/{user}/{repo}.git",
}
"""Built in url abbreviations for remote git repos."""

DEFAULT_EXCLUDE_DIRS = [".git", ".tox", ".cache"]
"""List of directory names to exclude by default when walking a folder recursively."""

DEFAULT_DOWNLOAD_CACHE_BASE = Path(os.path.expanduser("~/.cache/frkl/download_cache"))
"""Default cache location for downloads."""


DEFAULT_KEY_FORMATS = ["underscore"]
"""Default key formats for the Type-related helper methods/classes."""

REFERENCES_KEY = "references"
"""Default name for the 'references' key in a doc dict."""
DEFAULT_PLUGIN_MANAGER_CONFIG = {
    "supports_attribute": "_plugin_supports",
    "name_attribute": "_plugin_name",
}

FRKL_DEFAULT_SHARE_DIR = os.path.expanduser("~/.local/share/frkl")

FRKL_MANAGED_FILES_FOLDER = os.path.join(FRKL_DEFAULT_SHARE_DIR, "managed_files")

FRKL_MANAGED_FILES_ITEMS_FOLDER_NAME = "items"

FRKL_COLOR_PROGRESSION = ["blue", "green", "bright_magenta", "grey66", "deep_pink4"]
