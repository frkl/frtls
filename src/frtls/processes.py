# -*- coding: utf-8 -*-
import asyncio
import os
import re
import webbrowser
from asyncio.subprocess import Process
from shutil import which
from typing import Dict, Iterable, Optional


async def async_run(cmd: str, *args, wait=True) -> Process:

    env: Dict = dict(os.environ)  # make a copy of the environment
    lp_key = "LD_LIBRARY_PATH"  # for Linux and *BSD.
    lp_orig = env.get(lp_key + "_ORIG")  # pyinstaller >= 20160820 has this
    xdb_key = "XDG_DATA_DIRS"
    xdb_orig = env.get(xdb_key + "_ORIG")

    if lp_orig:
        env[lp_key] = lp_orig
    else:
        lp = env.get(lp_key, None)
        if lp is not None and re.search(r"/_MEI......", lp):
            env.pop(lp_key)
    if xdb_orig:
        env[xdb_key] = xdb_orig

    proc = await asyncio.create_subprocess_exec(
        cmd,
        *args,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        env=env,
    )

    if wait:
        await proc.wait()

    return proc


async def open_url_in_browser(url):

    if command_exists("xdg-open"):
        await async_run("xdg-open", url)
    else:
        webbrowser.open_new_tab(url)


def assemble_path(extra_path: Optional[Iterable[str]] = None) -> Iterable[str]:

    _path = os.environ.get("PATH", os.defpath).split(os.pathsep)

    if extra_path is not None:
        if isinstance(extra_path, str):
            extra_path = extra_path.split(os.pathsep)
        path = _path + list(extra_path)
    else:
        path = _path

    return path


def command_exists(command: str, extra_path: Optional[Iterable[str]] = None) -> bool:

    cmd = get_command_path(command, extra_path)

    return cmd is not None


def get_command_path(
    command: str, extra_path: Optional[Iterable[str]] = None
) -> Optional[str]:

    path = assemble_path(extra_path)
    cmd = which(command, path=os.pathsep.join(path))
    return cmd
