# -*- coding: utf-8 -*-
import logging

from frtls.subprocesses import AsyncSubprocess


log = logging.getLogger("frtls")


class GitProcess(AsyncSubprocess):
    def __init__(
        self,
        git_command: str,
        *args,
        working_dir: str = None,
        expected_ret_code=0,
        **env_vars
    ):

        git_args = [git_command]
        git_args.extend(args)
        super().__init__(
            "git",
            *git_args,
            working_dir=working_dir,
            expected_ret_code=expected_ret_code,
            **env_vars
        )
