# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, Any, Dict, Iterable, Mapping, Optional

from frtls.doc.explanation import Explanation
from rich.console import Console, ConsoleOptions, RenderResult


if TYPE_CHECKING:
    from frtls.tasks import Task, TaskDesc, Tasks


class TaskExplanation(Explanation):
    def __init__(self, data: Optional["Task"] = None, **kwargs):

        super().__init__(data=data, **kwargs)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        task: Task = self.data

        task_desc: TaskDesc = task.task_desc

        result: Dict[str, Any] = {
            "meta": {"id": task.id, "name": task_desc.name, "msg": task_desc.msg}
        }

        if hasattr(task, "children"):
            subtasks = []
            for child in task.children.values():  # type: ignore
                st_expl = TaskExplanation(child)
                subtasks.append(st_expl)

            result["subtasks"] = subtasks

        if task._postprocess_task is not None:
            postprocess_expl = TaskExplanation(task._postprocess_task)
            result["postprocess"] = postprocess_expl

        return result

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        # expl_data: Mapping[str, Any] = self.explanation_data
        yield self.explain("task")


class TaskResult(Explanation):
    def __init__(self, task: "Task", result_value: Any = None, **kwargs):

        self._task: Task = task

        super().__init__()

        if result_value is not None:
            self.result_value = result_value

    @property
    def result_value(self) -> Any:
        return self.data

    @result_value.setter
    def result_value(self, result_value: Any) -> None:
        if self.data is not None:
            raise Exception("Result value already set.")
        self.data = result_value

    @property
    def task(self) -> "Task":
        return self._task

    def get_processed_result(self) -> Any:

        return self.data

    async def create_explanation_data(self) -> Mapping[str, Any]:

        if self.get_processed_result() is None:
            r = "--no result--"
        else:
            r = self.get_processed_result()

        return {self._task.id: r}


class TasksResult(TaskResult):
    def __init__(self, task: "Tasks", result_value: Any = None):

        super().__init__(task=task, result_value=result_value)

    @property
    def child_tasks(self) -> Optional[Iterable["Task"]]:
        return self._task.children.values()  # type: ignore

    @property
    def child_results(self) -> Iterable["TaskResult"]:

        if not self.child_tasks:
            return []

        result = []
        for c in self.child_tasks:
            result.append(c.result)

        return result

    def get_child_result_values(self) -> Iterable[Any]:

        result = []
        for r in self.child_results:
            result.append(r.get_processed_result())
        return result

    def to_dict(self) -> Mapping[Any, Any]:

        return self.explanation_data

    async def create_explanation_data(self) -> Any:

        r = self.get_processed_result()

        result = {
            "task_name": self._task.task_desc.name,
            "result": r,
            "subtasks": self.child_results,
        }
        return result
