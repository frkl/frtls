# -*- coding: utf-8 -*-
from typing import TYPE_CHECKING, List, Optional

from frtls.exceptions import FrklException


if TYPE_CHECKING:
    from frtls.tasks import Task


def get_failed_msgs(task: "Task", msgs: Optional[List[str]] = None) -> List[str]:

    if msgs is None:
        msgs = []

    if not hasattr(task, "children"):

        if not task.success:
            _m = task.task_desc.get_failed_msg()
            if _m not in msgs:
                msgs.append(_m)
        return msgs

    else:

        for child in task.children.values():  # type: ignore
            _n = get_failed_msgs(child)
            msgs.extend(_n)

        return msgs


class FrklTasksException(FrklException):
    def __init__(self, task: "Task"):

        self._task: "Task" = task

        self._failed_childs = None

        msgs = get_failed_msgs(self._task)
        reason = None
        if msgs:
            reason = "\n".join(msgs)

        super().__init__(msg=task.task_desc.get_failed_msg(), reason=reason)

    @property
    def task(self) -> "Task":
        return self._task
