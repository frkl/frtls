# -*- coding: utf-8 -*-
pyinstaller = {
    "hiddenimports": [
        "frtls.tasks.task_watcher",
        "frtls.tasks.watchers.terminal",
        "frtls.tasks.watchers.simple",
        "frtls.tasks.task_watcher.watchers.tree",
    ]
}
