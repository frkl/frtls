# -*- coding: utf-8 -*-
from typing import Any

from frtls.exceptions import FrklException
from treelib import Node, Tree


def add_node_data_to_tree(
    tree: Tree,
    path: str,
    obj: Any,
    node: Node = None,
    overwrite=False,
    path_seperator=".",
):

    if node is None:
        node = tree.root
        if node is None:
            raise FrklException(
                msg=f"Can't add node to tree on path '{path}'.",
                reason="Tree has no root node.",
            )

    if tree.contains(path) and tree.get_node(path).data is not None and not overwrite:
        raise FrklException(
            msg=f"Can't add node '{path}' to tree.",
            reason="Node already exists and overwrite is set to False.",
        )

    tokens = path.split(path_seperator)
    current_path = []
    parent = node

    for token in tokens[0:-1]:
        current_path.append(token)
        current_path_string = path_seperator.join(current_path)
        new_parent = tree.get_node(current_path_string)
        if new_parent is None:
            new_parent = tree.create_node(
                tag=f"{token} (empty)",
                identifier=current_path_string,
                parent=parent,
                data=None,
            )
        parent = new_parent

    if tree.contains(path):
        n = tree.get_node(path)
        if n.data is None or overwrite:
            n.data = obj
            n.tag = tokens[-1]
        else:
            raise FrklException(
                msg=f"Can't add node to tree on path '{path}'",
                reason="Path already exists and has data attached to it.",
            )
    else:
        tree.create_node(tag=tokens[-1], identifier=path, parent=parent, data=obj)
