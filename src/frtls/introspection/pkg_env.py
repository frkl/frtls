# -*- coding: utf-8 -*-
import datetime
import importlib
import io
import json
import logging
import os
import pkgutil
import sys
import tempfile
from collections import Callable, Coroutine
from pathlib import Path
from types import ModuleType
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Set,
    Tuple,
)

import pkg_resources

from appdirs import AppDirs
from frtls.defaults import FRTLS_RESOURCES_FOLDER
from frtls.templating.jinja import process_string_template
from frtls.types import Singleton


IGNORE_PKGS = [
    "zipp",
    "yaspin",
    "yarl",
    "whichcraft",
    "wheel",
    "wcwidth",
    "watchgod",
    "watchdog",
    "vistir",
    "virtualenv",
    "validators",
    "uvloop",
    "urllib3",
    "typing-extensions",
    "typed-ast",
    "treelib",
    "traitlets",
    "tox",
    "tornado",
    "toml",
    "text-unidecode",
    "stevedore",
    "sortedcontainers",
    "sniffio",
    "smmap",
    "six",
    "setuptools",
    "setuptools-scm",
    "scp",
    "scalpl",
    "s3transfer",
    "ruamel.yaml",
    "ruamel.yaml.clib",
    "rfc3986",
    "requests",
    "regex",
    "pyyaml",
    "pyupdater",
    "pyupdater-scp-plugin",
    "pyupdater-s3-plugin",
    "pytz",
    "python-slugify",
    "python-dateutil",
    "pytest",
    "pytest-cov",
    "pypubsub",
    "pyparsing",
    "pynacl",
    "pymdown-extensions",
    "pyinstaller",
    "pygments",
    "pyflakes",
    "pydriller",
    "pydantic",
    "pycparser",
    "pycodestyle",
    "py",
    "ptyprocess",
    "prompt-toolkit",
    "pp-ez",
    "poyo",
    "portray",
    "pluggy",
    "pkg-resources",
    "pipdeptree",
    "pip",
    "pip-tools",
    "pickleshare",
    "pexpect",
    "pdocs",
    "pbr",
    "pathtools",
    "pathspec",
    "parso",
    "paramiko",
    "packaging",
    "nuitka",
    "nodeenv",
    "nltk",
    "mypy",
    "mypy-extensions",
    "multidict",
    "more-itertools",
    "mkdocs",
    "mkdocs-material",
    "mccabe",
    "markupsafe",
    "markdown",
    "mako",
    "lunr",
    "logzero",
    "lizard",
    "livereload",
    "kubernetes-asyncio",
    "jmespath",
    "jinja2",
    "jinja2-time",
    "jedi",
    "isort",
    "ipython",
    "ipython-genutils",
    "importlib-resources",
    "importlib-metadata",
    "immutables",
    "idna",
    "idna-ssl",
    "identify",
    "hyperframe",
    "hug",
    "httpx",
    "hstspreload",
    "hpack",
    "h11",
    "h2",
    "gitpython",
    "gitdb",
    "future",
    "formic2",
    "flake8",
    "filelock",
    "fastavro",
    "falcon",
    "examples",
    "entrypoints",
    "ed25519",
    "dsdev-utils",
    "dpath",
    "docutils",
    "distlib",
    "dictdiffer",
    "decorator",
    "dataclasses",
    "cryptography",
    "cruft",
    "coverage",
    "cookiecutter",
    "contextvars",
    "colorama",
    "click",
    "click-aliases",
    "chardet",
    "cfgv",
    "cffi",
    "certifi",
    "pre-commit",
    "bsdiff4",
    "botocore",
    "boto3",
    "blessed",
    "black",
    "binaryornot",
    "bcrypt",
    "backcall",
    "attrs",
    "async-timeout",
    "async-generator",
    "async-exit-stack",
    "asgiref",
    "arrow",
    "appdirs",
    "anyio",
    "altgraph",
    "aiohttp",
    "frkl.jinja2schema",
    "asyncclick",
]


log = logging.getLogger("frtls")

DEFAULT_EXCLUDE_DIRS = [".git", ".tox", ".cache"]


# -----------------------------------------------------------
# helper methods
def get_entry_point_imports_hook(
    entry_points: List[str],
) -> Tuple[Mapping[str, str], Set]:

    hook_ep_packages: Dict[str, List[str]] = dict()
    hiddenimports = set()

    for ep_package in entry_points:

        for ep in pkg_resources.iter_entry_points(ep_package):

            if ep_package in hook_ep_packages.keys():
                package_entry_point = hook_ep_packages[ep_package]
            else:
                package_entry_point = []
                hook_ep_packages[ep_package] = package_entry_point
            package_entry_point.append(
                "{} = {}:{}".format(ep.name, ep.module_name, ep.attrs[0])
            )
            hiddenimports.add(ep.module_name)

    hooks = {}
    hooks[
        "pkg_resources_hook.py"
    ] = """# Runtime hook generated from spec file to support pkg_resources entrypoints.
ep_packages = {}

if ep_packages:
    import pkg_resources
    default_iter_entry_points = pkg_resources.iter_entry_points

    def hook_iter_entry_points(group, name=None):
        if group in ep_packages and ep_packages[group]:
            eps = ep_packages[group]
            for ep in eps:
                parsedEp = pkg_resources.EntryPoint.parse(ep)
                parsedEp.dist = pkg_resources.Distribution()
                yield parsedEp
        else:
            yield default_iter_entry_points(group, name)
            return

    pkg_resources.iter_entry_points = hook_iter_entry_points

""".format(
        hook_ep_packages
    )

    log.debug(f"Retrieved hooks: {hooks}")
    log.debug(f"Retrieved hidden imports: {hiddenimports}")

    return hooks, hiddenimports


def get_datas(
    resources_map: Mapping[str, List[str]], exclude_dirs: Optional[Iterable[str]] = None
):

    if exclude_dirs is None:
        exclude_dirs = DEFAULT_EXCLUDE_DIRS

    datas = []

    for package, files in resources_map.items():

        proot = os.path.dirname(importlib.import_module(package).__file__)
        for f in files:
            src = os.path.join(proot, f)
            if not os.path.isdir(os.path.realpath(src)):
                data = (src, package)
                datas.append(data)
            else:

                for root, dirnames, filenames in os.walk(src, topdown=True):

                    if exclude_dirs:
                        dirnames[:] = [d for d in dirnames if d not in exclude_dirs]

                    for filename in filenames:

                        path = os.path.join(root, filename)
                        rel_path = os.path.join(
                            package, os.path.dirname(os.path.relpath(path, proot))
                        )
                        data = (path, rel_path)
                        datas.append(data)

    log.debug(f"Retrieved pkg data: {datas}")
    return datas


def get_analysis_args(
    scripts: List[Mapping[str, str]],
    resources_map: Mapping[str, List[str]],
    hiddenimports: List[str],
    hooks_path: List[str],
    entry_points: Optional[List[str]] = None,
    app_details: Optional[Mapping[str, Any]] = None,
    block_cipher=None,
):

    tempdir = tempfile.mkdtemp(prefix="pkg_build")
    datas = get_datas(resources_map=resources_map)
    if entry_points is None:
        entry_points = []
    ep_hooks, auto_imports = get_entry_point_imports_hook(entry_points=entry_points)
    runtime_hooks = []
    for filename, hook_string in ep_hooks.items():
        path = os.path.join(tempdir, filename)
        with io.open(path, "w", encoding="utf-8") as f:
            f.write(hook_string)
        runtime_hooks.append(path)

    sc = create_entry_point_from_template(tempdir, scripts)

    if app_details is not None:
        app_details_file = os.path.join(tempdir, "app.json")
        with open(app_details_file, "w") as f:
            json.dump(app_details, f)
        d = (app_details_file, ".")
        datas.append(d)

    kwargs = dict(
        scripts=sc,
        pathex=[],
        binaries=[],
        datas=datas,
        hiddenimports=list(auto_imports) + hiddenimports,
        hookspath=hooks_path,
        runtime_hooks=runtime_hooks,
        excludes=["FixTk", "tcl", "tk", "_tkinter", "tkinter", "Tkinter"],
        win_no_prefer_redirects=False,
        win_private_assemblies=False,
        cipher=block_cipher,
        noarchive=False,
    )

    log.debug(f"Created analysis args: {kwargs}")

    return kwargs


def create_entry_point_from_template(
    working_dir: str, scripts: List[Mapping[str, str]]
):

    if not scripts:
        raise Exception("No scripts provided.")

    template = Path(os.path.join(FRTLS_RESOURCES_FOLDER, "entry_point.py.j2"))
    template_string = template.read_text()

    replaced = process_string_template(
        template_string=template_string, replacement_dict={"scripts": scripts}
    )

    target = Path(os.path.join(working_dir, "cli.py"))
    target.write_text(replaced)

    return [target.resolve().as_posix()]


def create_pkg_metadata(
    raw: Mapping[str, Any], pkgs_for_scripts: Iterable[str] = None
) -> Mapping[str, Any]:

    result: Dict[str, Any] = {
        "hiddenimports": [],
        "entry_points": [],
        "resources_map": {},
        "scripts": {},
        "hooks_path": [],
    }
    for pkg, properties in raw.items():

        result["hiddenimports"].extend(properties["hiddenimports"])
        result["entry_points"].extend(properties["entry_points"])
        result["scripts"].update(properties["scripts"])
        result["resources_map"][pkg] = properties["resources"]
        result["hooks_path"].extend(properties["hooks_path"])

    if pkgs_for_scripts is None:
        pkgs_for_scripts = raw.keys()

    for entry_point in pkg_resources.iter_entry_points("console_scripts"):
        script_name = entry_point.name
        if script_name in result["scripts"].keys():
            continue

        module_name = entry_point.module_name
        pkg_name = module_name.split(".")[0]
        attr = entry_point.attrs[0]

        if pkg_name not in pkgs_for_scripts:
            continue

        result["scripts"][script_name] = {"module_name": module_name, "attr": attr}

    log.debug(f"Created pkg metadata: {result}")

    return result


def find_pkg_versions(pkgs: Mapping[str, Any]) -> Mapping[str, str]:

    _versions = {}
    for pkg_name, details in pkgs.items():

        try:
            mod = importlib.import_module(pkg_name)
            version = getattr(mod, "__version__")
        except (Exception) as e:
            log.debug(f"Can't add version for pkg '{pkg_name}': {e}")
            version = "n/a"

        _versions[pkg_name] = version

    return _versions


def find_installed_frkl_pkgs(
    ignore_pkgs: Optional[Iterable[str]] = None,
    only_pkgs: Optional[Iterable[str]] = None,
) -> Mapping[str, Any]:

    if ignore_pkgs is None:
        ignore_pkgs = IGNORE_PKGS

    installed_packages = pkg_resources.working_set

    metadata_modules = []
    for i in installed_packages:

        pkg_name = i.key

        if pkg_name in ignore_pkgs:
            continue

        if only_pkgs is not None:
            if pkg_name not in only_pkgs:
                continue

        log.debug(f"querying package: {i}")

        if not hasattr(sys, "frozen"):

            try:
                _pkg_name = pkg_name.replace("-", "_")
                mod = __import__(f"{_pkg_name}._meta", fromlist="dummy")
                metadata_modules.append(mod)
            except (Exception):
                pass

        else:
            toc: Any = set()
            for importer in pkgutil.iter_importers(i.key):
                log.debug(f"found importer: {importer}")
                if hasattr(importer, "toc"):
                    toc |= importer.toc
            log.debug(f"toc: {toc}")
            for name in toc:
                log.debug(f"name: {name}")
                if name == "_meta":
                    m = __import__(name, fromlist="dummy")
                    log.debug(f"imported module: {m}")
                    metadata_modules.append(m)
                else:
                    log.debug("no '_meta' child")

    if not metadata_modules:
        return {}

    result: Dict[str, Any] = {}

    for mod in metadata_modules:

        pyinstaller_data = getattr(mod, "pyinstaller", {})
        name = mod.__name__.split(".")[0]
        result[name] = {}
        for prop in [
            "resources",
            "hiddenimports",
            "entry_points",
            "scripts",
            "hooks_path",
        ]:
            result[name][prop] = pyinstaller_data.get(prop, [])

        mod_path = mod.__file__

        mod_parent = os.path.dirname(mod_path)
        resources_folder = os.path.join(mod_parent, "resources")
        if (
            os.path.exists(resources_folder)
            and resources_folder not in result[name]["resources"]
        ):
            result[name]["resources"].append(resources_folder)
        version_file = os.path.join(mod_parent, "version.txt")
        if (
            os.path.exists(version_file)
            and version_file not in result[name]["resources"]
        ):
            result[name]["resources"].append(version_file)
        frkl_file = os.path.join(mod_parent, "frkl.info")
        if os.path.exists(frkl_file) and frkl_file not in result[name]["resources"]:
            result[name]["resources"].append(frkl_file)

        if os.path.basename(mod_path) == "_meta.py":
            continue

        runtime_hooks = os.path.join(os.path.dirname(mod_path), "pyinstaller_hooks")
        if (
            os.path.isdir(runtime_hooks)
            and runtime_hooks not in result[name]["hooks_path"]
        ):
            result[name]["hooks_path"].append(runtime_hooks)

    log.debug(f"Found packages: {result}")

    return result


# -----------------------------------------------------------
# main class
class PythonEnvMetadata(object):
    def __init__(
        self, project_dir: Optional[str] = None, main_pkg: Optional[str] = None
    ):

        if project_dir is None:
            project_dir = os.getcwd()

        self._project_dir: str = project_dir

        src_path: str = os.path.join(self._project_dir, "src")
        if not os.path.isdir(src_path):
            raise Exception(f"No source path: {src_path}")

        if main_pkg is None:
            childs = []
            for child in os.listdir(src_path):
                if child.endswith("egg-info"):
                    continue
                if "__" in child:
                    continue
                full = os.path.join(src_path, child)
                if not os.path.isdir(full):
                    continue

                childs.append(child)

            if len(childs) == 0:
                raise Exception(f"No default module found in source path '{src_path}'.")
            elif len(childs) > 1:
                if os.path.basename(self._project_dir) in childs:
                    main_pkg = os.path.basename(self._project_dir)
                else:
                    raise Exception(
                        f"No unique default module found in source path '{src_path}': {childs}"
                    )
            else:
                main_pkg = childs[0]

        self._main_pkg = main_pkg

        self._pkg_meta: MutableMapping[str, Any] = get_pkg_meta(self._main_pkg)
        self._app_name: str = self._pkg_meta.get(
            "app_name", os.path.basename(self._project_dir)
        )

        # if self._config.get("ignore_pkgs", None) is None:
        #     self._config["ignore_pkgs"] = []

        self._pkgs: Mapping[str, Any] = find_installed_frkl_pkgs()
        self._metadata: Mapping[str, Any] = create_pkg_metadata(self._pkgs)

        if not self._metadata.get("scripts", []):
            raise Exception(f"No scripts provided. Metadata: {self._metadata}")

        versions = find_pkg_versions(self._pkgs)
        app_details = {
            "app_name": self._app_name,
            "main_pkg": self._main_pkg,
            "versions": versions,
            "meta": self._pkg_meta,
            "build_time": datetime.datetime.now().strftime("%m.%d.%Y, %H:%M:%S"),
        }
        self._analysis_data = get_analysis_args(
            app_details=app_details, **self._metadata
        )

    @property
    def app_name(self) -> str:
        return self._app_name

    @property
    def main_pkg(self) -> str:
        return self._main_pkg

    @property
    def pkg_meta(self) -> Mapping[str, Any]:

        return self._pkg_meta

    @property
    def analysis_data(self):

        return self._analysis_data


def get_pkg_meta(pkg_name: str) -> MutableMapping[str, Any]:

    try:
        meta = importlib.import_module(f"{pkg_name}._meta")
    except (Exception):
        log.debug(
            f"Pkg '{pkg_name}' does not have a '_meta' module, returning empty dict..."
        )
        return {}

    config = {}
    for k in dir(meta):

        if k.startswith("_"):
            continue

        attr = getattr(meta, k)
        if isinstance(
            attr, (ModuleType, type, Callable, Coroutine)  # type: ignore
        ) or str(attr).startswith("typing."):
            continue

        config[k] = attr

    return config


class AppEnvironment(metaclass=Singleton):

    _app_name: Optional[str] = None
    _main_pkg: Optional[str] = None
    _versions: Optional[Mapping[str, str]] = None
    _build_time: Optional[str] = None
    _package_meta: Optional[Mapping[str, str]] = None
    _package_defaults: Optional[Mapping[str, Any]] = None
    _pkgs = None
    _globals: Dict[str, Any] = {}

    def __init__(self) -> None:

        if AppEnvironment._app_name is not None:
            return

        if not hasattr(sys, "frozen"):
            log.debug("Creating AppEnvironment object (not frozen).")
            app_details: Dict[str, Any] = {}

            app_details["app_name"] = os.path.basename(sys.argv[0])

            entry_point = None

            for cs in pkg_resources.iter_entry_points("console_scripts"):
                if cs.name == app_details["app_name"]:
                    entry_point = cs
                    break

            if entry_point is not None:

                app_details["main_pkg"] = entry_point.module_name.split(".")[0]
            else:
                pass
                # raise Exception(
                #     f"Can't determine main pkg for '{app_details['app_name']}'."
                # )

        else:
            log.debug("Creating AppEnvironment object (frozen).")
            app_details_file = os.path.join(sys._MEIPASS, "app.json")  # type: ignore
            if os.path.exists(app_details_file):
                log.debug(f"'app.json' file exists: {app_details_file}")
                with open(app_details_file, "r") as f:
                    app_details = json.load(f)
            else:
                raise Exception(f"No 'app.json' file: {app_details_file}")

        AppEnvironment._app_name = app_details.get("app_name")
        AppEnvironment._main_pkg = app_details.get("main_pkg")
        AppEnvironment._versions = app_details.get("versions", None)
        AppEnvironment._build_time = app_details.get("build_time", None)

        AppEnvironment._package_meta = app_details.get("meta", None)

    @property
    def app_name(self) -> str:
        if AppEnvironment._app_name is None:
            raise Exception(
                "AppEnvironment value 'app_name' not set (yet). This is a bug."
            )
        return AppEnvironment._app_name

    @property
    def main_pkg(self) -> str:
        if AppEnvironment._main_pkg is None:
            raise Exception(
                "AppEnvironment value 'main_pkg' not set (yet). This is a bug."
            )
        return AppEnvironment._main_pkg

    @property
    def pkg_meta(self) -> Mapping[str, Any]:

        if AppEnvironment._package_meta is None:
            AppEnvironment._package_meta = get_pkg_meta(self.main_pkg)
        return AppEnvironment._package_meta

    @property
    def version(self):

        return self.versions[self.main_pkg]

    @property
    def pkgs(self):

        if AppEnvironment._pkgs is not None:
            return self._pkgs

        AppEnvironment._pkgs = find_installed_frkl_pkgs()
        log.debug(f"pkgs for main pkg '{self.main_pkg}': {AppEnvironment._pkgs}")
        return AppEnvironment._pkgs

    @property
    def build_time(self) -> Optional[str]:

        return self._build_time

    @property
    def versions(self) -> Mapping[str, str]:

        if AppEnvironment._versions is not None:
            return AppEnvironment._versions

        AppEnvironment._versions = find_pkg_versions(self.pkgs)
        log.debug(
            f"versions for main pkg '{self.main_pkg}': {AppEnvironment._versions}"
        )

        return AppEnvironment._versions

    def get_pkg_defaults(self) -> Mapping[str, Any]:

        if AppEnvironment._package_defaults is not None:
            return AppEnvironment._package_defaults

        try:
            defaults = importlib.import_module(".".join([self.main_pkg, "defaults"]))
        except (Exception) as e:
            log.warning(f"Can't retrieve defaults module for '{self.main_pkg}': {e}")
            return {}

        result = {}
        for k in dir(defaults):
            if k.startswith("_"):
                continue

            attr = getattr(defaults, k)
            if isinstance(attr, (ModuleType, type)) or str(attr).startswith("typing."):
                continue

            result[k] = attr

        AppEnvironment._package_defaults = result

        return AppEnvironment._package_defaults

    def get_pkg_metadata_value(
        self, key: str, default: Optional[Any] = "__raise_exception__"
    ) -> Any:

        if key not in self.pkg_meta.keys():
            if default == "__raise_exception__":
                raise Exception(
                    f"No metadata value '{key}' for app '{self.app_name}' available. This is a bug in the packaging of this app."
                )
            else:
                return default

        value = self.pkg_meta[key]
        return value

    def get_app_dirs(self) -> Optional[AppDirs]:

        for k, v in self.get_pkg_defaults().items():
            if isinstance(v, AppDirs):
                return v

        return None

    def get_resources_folder(self) -> str:

        if "resources" in self.pkg_meta.keys():
            return self.pkg_meta["resources"]

        for k in self.get_pkg_defaults().keys():
            if "RESOURCES_FOLDER" in k:
                return self.get_pkg_defaults()[k]

        raise Exception(
            f"Can't deterimine resources folder for application '{self.app_name}'."
        )

    def set_global(self, key: str, value: Any) -> None:

        self._globals[key] = value

    def get_global(self, key: str) -> Any:

        return self._globals.get(key, None)


def process_pkg_metadata(project_dir: Optional[str] = None):

    md = PythonEnvMetadata(project_dir=project_dir)
    return md.analysis_data


if __name__ == "__main__":

    md = process_pkg_metadata()

    json_md = json.dumps(md)
    print(json_md)
