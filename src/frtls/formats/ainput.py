# -*- coding: utf-8 -*-
import logging
from typing import Any, Dict

import dpath
from anyio import aopen
from frtls.downloads import download_cached_text_file_async
from frtls.formats.input_formats import (
    auto_convert_string,
    determine_input_type,
    parse_file_content,
)
from frtls.strings import expand_git_url


log = logging.getLogger("frtls")


async def get_content_async(
    input_obj: Any,
    input_type: str = None,
    content_type: str = "auto",
    dict_string_separator: str = None,
    value_key=None,
    type_key="type",
    metadata_key="metadata",
) -> Any:
    """Auto determine content of the given input object.

    Automatically reads/downloads content in case the input object is a path or url string. If 'value_key' is not provided,
    the return value is the parsed content of the input object (converted to a dict or list if possible). If provided,
    the result is a dict that includes metadata about the input object and parsing process.

    Args:
        - *input_obj*: the input object
        - *input_type*: the input type
        - *content_type*: the type of the content (e.g. 'yaml', will be auto determined if not provided) - only applies to input types that have content, like a file
        - *dict_string_separator*: if not None, this method will try to convert a string to a single-key dict using the provided separator
        - *value_key*: if set, return a dict using this key for the content value
        - *type_key*: if 'value_key' is set, use this key to describe the content type
        - *metadata_key*: if 'value_key' is set, use this key for the content and parse process metadata

    Returns:
        the content as a dict (if possible), or string, optionally wrapped in a dict containing metadata about the content and parse process
    """

    if input_type is None:
        input_type = determine_input_type(input_obj)

    value = None
    metadata: Dict[str, Any] = {}
    if input_type is None:
        value = input_obj
        value_type = "unknown"
    elif input_type == "string":
        if dict_string_separator:
            if dict_string_separator in input_obj:
                key, v = input_type.split(dict_string_separator, maxsplit=2)
                v = auto_convert_string(v)
                d: Dict[str, Any] = {}
                if "." in key:
                    dpath.util.new(d, key, v, separator=".")
                else:
                    d[key] = v
                value = d
                value_type = "dict"
            else:
                value = input_obj
                value_type = "string"
                # raise Exception(f"Input string can't be converted to dict with separator '{dict_string_separator}'.")
        else:
            value = input_obj
            value_type = "string"
    elif input_type == "dict":
        value = input_obj
        value_type = "dict"
    elif input_type == "list":
        value = input_obj
        value_type = "list"
    elif input_type == "local_file":
        async with await aopen(input_obj) as f:
            content = await f.read()
        try:
            value, value_type = parse_file_content(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = "string"
            metadata["parse_successful"] = False
    elif input_type == "local_dir":
        raise Exception("Can't get content from local directory.")
    elif input_type == "git_repo":
        raise Exception("Can't get content from git repository.")
    elif input_type == "remote_url":
        url = expand_git_url(
            input_obj
        )  # doesn't need to be git url, but just in case...
        metadata["full_url"] = url
        content = await download_cached_text_file_async(url, return_content=True)
        try:
            value, value_type = parse_file_content(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = "string"
            metadata["parse_successful"] = False

    if not value_key:
        return value

    result = {}
    result[value_key] = value

    if type_key is not None:
        result[type_key] = value_type

    if metadata_key is not None:
        result[metadata_key] = metadata

    return result
