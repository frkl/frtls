# -*- coding: utf-8 -*-
import json
import os
import pprint
import re
import shutil
import textwrap
import time
from collections import OrderedDict
from collections.abc import Mapping
from pathlib import Path
from typing import Any, List, Optional, Sequence, Union

from colorama import Style
from frtls.cli.terminal import create_terminal
from frtls.const import OutputFormat
from frtls.exceptions import FrklException
from frtls.strings import reindent
from ruamel import yaml as ruamel_yaml
from ruamel.yaml import YAML, RoundTripRepresenter
from ruamel.yaml.comments import CommentedMap
from ruamel.yaml.compat import StringIO


class StringYAML(YAML):
    """Wraps :class:~YAML to be able to dump a string from a yaml object.

    More details: http://yaml.readthedocs.io/en/latest/example.html#output-of-dump-as-a-string

    Args:
        **kwargs (dict): arguments for the underlying :class:~YAML class
    """

    def __init__(self, **kwargs):
        super(StringYAML, self).__init__(**kwargs)

    def dump(self, data, stream=None, split_multiline_strings=True, **kw):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        if split_multiline_strings:
            ruamel_yaml.scalarstring.walk_tree(data)

        YAML.dump(self, data, stream, **kw)
        if inefficient:
            return stream.getvalue()


class IgnoreAliasRepresenter(RoundTripRepresenter):
    def __init__(self, default_style=None, default_flow_style=None, dumper=None):

        super(IgnoreAliasRepresenter, self).__init__(
            default_style=default_style,
            default_flow_style=default_flow_style,
            dumper=dumper,
        )

    def ignore_aliases(self, data):
        return True


def serialize(
    python_object: Any,
    format: Union[OutputFormat, str] = "raw",
    target: Optional[Union[str, Mapping]] = None,
    safe=False,
    indent=0,
    sort_keys=False,
    ignore_aliases=False,
    yaml_representers=None,
    strip: bool = False,
):
    """Utility method to print out readable strings from python objects (mostly dicts).

    Args:
        python_object (obj): the object to print
        format (str): the format of the output (available: 'yaml', 'json', 'raw', and 'pformat')
        target:
        safe (bool): whether to use a 'safe' way of converting to string (if available in the output format type)
        indent (int): the indentation (optional)
        sort_keys (boolean): whether to sort a (root-level) dictionary (only works for YAML so far)
        strip (bool): whether to strip the result before indenting
    """

    if format is None:
        format = OutputFormat.RAW

    if isinstance(format, str):
        try:
            format = OutputFormat(format)
        except (Exception):

            raise Exception(
                f"No valid output format provided. Supported: {', '.join([e.value for e in OutputFormat])}"
            )

    if format == OutputFormat.YAML:

        if isinstance(python_object, OrderedDict) or (
            sort_keys and isinstance(python_object, Mapping)
        ):
            temp = CommentedMap()
            if sort_keys:
                for k in sorted(python_object):
                    temp[k] = python_object[k]
            else:
                for k in python_object:
                    temp[k] = python_object[k]
        else:
            temp = python_object

        if safe:
            ryaml = StringYAML(typ="safe")
            if yaml_representers:
                for k, v in yaml_representers.items():
                    ryaml.representer.add_representer(k, v)
            ryaml.default_flow_style = False
            output_string = ryaml.dump(temp)
            # output_string = yaml.safe_dump(
            # python_object,
            # default_flow_style=False,
            # encoding='utf-8',
            # allow_unicode=True)
        else:
            ryaml = StringYAML()
            if yaml_representers:
                for k, v in yaml_representers.items():
                    ryaml.representer.add_representer(k, v)
            if ignore_aliases:
                ryaml.Representer = IgnoreAliasRepresenter
            ryaml.default_flow_style = False
            output_string = ryaml.dump(temp)

            # output_string = yaml.dump(
            # python_object,
            # default_flow_style=False,
            # encoding='utf-8',
            # allow_unicode=True)
    elif format == OutputFormat.JSON:
        output_string = json.dumps(python_object, sort_keys=sort_keys, indent=2)
    elif format == OutputFormat.JSON_LINE:
        output_string = json.dumps(python_object, sort_keys=sort_keys, indent=None)
    elif format == OutputFormat.RAW:
        output_string = str(python_object)
    elif format == OutputFormat.PFORMAT:
        output_string = pprint.pformat(python_object)

    if strip:
        output_string = output_string.strip()

    if indent != 0:
        output_string = reindent(output_string, indent)

    if target:
        if isinstance(target, str):
            write_string_to(output_string, target=target)
        elif isinstance(target, Mapping):
            target_val = target.get("target", None)
            target_type = target.get("target_type", None)
            target_opts = target.get("target_opts", {})
            write_string_to(
                output_string, target=target_val, target_type=target_type, **target_opts
            )
        else:
            raise TypeError(
                f"'target' value, needs to be either str or Mapping, not: {type(target)}"
            )

    return output_string


def write_string_to(
    content: str, target: Any = None, target_type: str = "auto", **target_opts: Any
):

    if (target_type is None or target_type == "auto") and target is None:
        target_type = "return-value"
    elif target_type is None or target_type == "auto":
        target_type = "file"

    if target_type == "return-value":
        return content

    if target_type != "file":
        raise NotImplementedError(
            "Only 'file' and 'return-value' write target types supported at this stage."
        )

    target_config = {"force": False}
    target_config.update(target_opts)

    path = Path(target).resolve()  # type: ignore

    if path.exists():
        if path.is_dir():
            raise FrklException(
                msg=f"Can't write content to path '{path.as_posix()}'",
                reason="Target is a directory.",
            )
        if not target_config["force"]:
            raise FrklException(
                msg=f"Can't write content to path '{path.as_posix()}'",
                reason="Target exists and 'force' set to 'False'.",
            )

    path.write_text(content)

    return {"target": path}


def create_two_column_table(
    data,
    header: Optional[List[str]] = None,
    tablefmt: str = "plain",
    alternate_row_colors=True,
) -> str:

    import tabulate

    terminal = create_terminal()

    max_width = 0
    for f_name in data.keys():
        w = len(f_name)
        if w > max_width:
            max_width = w

    rest_width = terminal.width - max_width - 3
    max_rest = 20

    new_data = []
    toggle = True
    for key in data.keys():
        val = data[key]

        if rest_width > max_rest:
            val = "\n".join(textwrap.wrap(val, rest_width))

        if toggle and alternate_row_colors:
            key = Style.DIM + key + Style.NORMAL
            val = Style.DIM + val + Style.NORMAL
            # key = f"{terminal.dim}{key}{terminal.normal}"
            # val = f"{terminal.dim}{val}{terminal.normal}"

        toggle = not toggle
        new_data.append([key, val])

    if rest_width > max_rest:

        _headers: Optional[List] = None
        if header:
            _headers = [
                Style.BRIGHT + header[0] + Style.RESET_ALL,
                Style.BRIGHT + header[1] + Style.RESET_ALL,
            ]

            result = tabulate.tabulate(new_data, headers=_headers, tablefmt=tablefmt)
        else:
            result = tabulate.tabulate(new_data, tablefmt=tablefmt)

    else:
        result = ""
        for row in new_data:
            if header:
                header_one = header[0]
                header_two = header[1]
                header_one_str = f"{Style.DIM}{header_one}{Style.RESET_ALL}"
                result = f"{result}{Style.BRIGHT}{header_one_str}{row[0]}\n"
                result = (
                    f"{result}{Style.BRIGHT}{header_two}{Style.RESET_ALL}{row[1]}\n\n"
                )
            else:
                result = f"{result}{row[0]}\n"
                result = f"{result}{row[1]}\n\n"

    return result


def create_multi_column_table(data, headers) -> str:

    import tabulate

    terminal = create_terminal()

    max_width = 0
    for row in data:
        w = 0
        for index, word in enumerate(row[0:-1]):

            header_len = len(headers[index])
            wl = len(str(word))
            if header_len > wl:
                wl = header_len
            w = w + wl + 3
        if w > max_width:
            max_width = w

    rest_width = terminal.width - max_width - 3
    max_rest = 20

    new_data = []
    toggle = True
    for row in data:
        val = row[-1]

        if rest_width > max_rest:
            val = "\n".join(textwrap.wrap(val, rest_width))

        if toggle:
            new_row = []
            for word in row[0:-1]:
                new_row.append(Style.DIM + str(word) + Style.RESET_ALL)
            new_row.append(Style.DIM + val + Style.RESET_ALL)
            row = new_row
        else:
            new_row = row[0:-1]
            new_row.append(val)
            row = new_row

        toggle = not toggle
        new_data.append(row)

    result = ""

    if rest_width > max_rest:

        new_headers = []
        for h in headers:
            new_headers.append(Style.BRIGHT + h + Style.RESET_ALL)

        result = tabulate.tabulate(new_data, headers=new_headers)
    else:
        result = ""
        for row in new_data:

            for index, word in enumerate(row):
                header_str = "{}{}{}".format(Style.DIM, headers[index], Style.RESET_ALL)
                result = (
                    f"{result}{Style.BRIGHT}{header_str}{Style.NORMAL}{header_str}\n"
                )

    return result


def delete_section_in_text_file(
    path: Union[str, Path],
    prefix: Sequence[str],
    postfix: Sequence[str],
    backup: bool = True,
) -> None:

    ensure_section_in_text_file(
        path=path,
        section_content="",
        prefix=prefix,
        postfix=postfix,
        backup=backup,
        add_pre_and_postfix=False,
    )


def ensure_section_in_text_file(
    path: Union[str, Path],
    section_content: str,
    prefix: Sequence[str],
    postfix: Sequence[str],
    backup: bool = True,
    add_pre_and_postfix: bool = True,
) -> None:
    """Makes sure a section of text is contained within a text file, between one or several lines of pre- and postfix content.

    The pre- and postfix content is used as a unique marker to signal to this function whether this or (a version of) this content
    was added before.
    """

    if isinstance(path, Path):
        path = path.resolve().as_posix()

    if not os.path.exists(path):
        content = ""
    else:
        with open(path, "r") as f:
            content = f.read() + "\n"

    pattern_string = ""
    content_string = ""
    for line in prefix[0:-1]:
        pattern_string = pattern_string + line + r"(?:\n|\r|\r\n?)"
        if add_pre_and_postfix:
            content_string = content_string + line + "\n"

    pattern_string = pattern_string + prefix[-1]
    if add_pre_and_postfix:
        content_string = content_string + prefix[-1] + "\n"

    pattern_string = pattern_string + r"[\s\S]*"
    content_string = content_string + section_content + "\n"

    for line in postfix:
        pattern_string = pattern_string + line + r"(?:\n|\r|\r\n?)"
        if add_pre_and_postfix:
            content_string = content_string + line + "\n"

    pattern = re.compile(pattern_string)
    result = re.search(pattern, content)

    if not result:
        if content_string:
            c = f"{content}{content_string}".strip() + "\n"
            with open(path, "w") as f:
                f.write(c)

    else:
        replaced = re.sub(pattern, content_string, content)

        if replaced == content:
            return

        if backup:
            timestamp = time.strftime("%Y%m%d-%H%M%S")
            backup_name = f"{path}.{timestamp}.bak"
            shutil.copy2(path, backup_name)

        with open(path, "w") as f:
            f.write(replaced.strip() + "\n")
