# -*- coding: utf-8 -*-
import json
import logging
from collections import namedtuple
from collections.abc import Sequence
from enum import Enum
from pathlib import Path
from typing import Any, Dict, Mapping, MutableMapping, Optional, Tuple, Union

import dpath
import toml
from frtls.downloads import download_cached_text_file
from frtls.exceptions import FrklParseException
from frtls.strings import expand_git_url, is_git_repo_url, is_url_or_abbrev
from ruamel.yaml import YAML


log = logging.getLogger("frtls")


def auto_parse_dict_string(
    content: str,
    content_type: Optional[str] = "auto",
    default_if_empty: Optional[Any] = None,
    content_origin: Any = None,
) -> MutableMapping:
    """Try to automatically parse a string into a Python object.

    If *content_type* is set to 'auto', this will try to parse the provided string as (in this order):

        - *yaml*
        - "json*
        - "toml*

    Optionally, you can provide the *content_origin* argument. This will be used in the execption that is thrown
    should anything go wrong. If the *content_origin* has an attribute 'id', that attribute will be used in
    the exception message instead of the *content_origin* object itself.

    Args:

        - *content*: the content to parse
        - *content_type*: the content type, options: 'auto', 'yaml', 'json', 'toml'
        - *default_if_empty*: the value to return if the content is empty
        - *content_origin*: the content origin
    """

    loaded = None

    if content_type is None:
        content_type = "auto"

    if not isinstance(content_type, str):
        raise Exception("Input is not a string, but '{}'".format(type(content)))

    if not content:
        if not default_if_empty:
            default_if_empty = {}
        return default_if_empty

    possible_exceptions = {}

    def solution_msg():
        if content_origin is not None and hasattr(content_origin, "id"):
            solution = "Check format of content of: {}".format(content_origin.id)
        else:
            solution = "Check format of content."
        return solution

    if content_type == "auto":
        if not content.strip()[0] == "{" and not content.strip()[0] == "[":
            content_type = "yaml"
            try:
                yaml = YAML()
                loaded = yaml.load(content)
            except (Exception) as e:
                content_type = None
                possible_exceptions["yaml"] = e
        if loaded is None and (content.strip()[0] == "{" or content.strip()[0] == "["):
            content_type = "json"
            try:
                loaded = json.loads(content)
            except (Exception) as e:
                content_type = None
                possible_exceptions["json"] = e

        if loaded is None:
            content_type = "toml"
            try:
                loaded = toml.loads(content)
            except (Exception) as e:
                content_type = None
                possible_exceptions["toml"] = e

        if loaded is None:
            solution = solution_msg()
            # print(possible_exceptions)
            raise FrklParseException(
                content=content,
                msg="Could not parse raw metadata.",
                reason="Unknown format, tried 'yaml', 'json', and 'toml'.",
                solution=solution,
                content_origin=content_origin,
                exception_map=possible_exceptions,
            )
        if content_origin is not None and hasattr(content_origin, "id"):
            log.debug(
                "Detected content type for '{}': {}".format(
                    content_origin.id, content_type
                )
            )
        else:
            if len(content) > 31:
                snippet = (content[0:30] + "...").replace("\n", " ")
            else:
                snippet = content
            log.debug(
                "Detected content type for '{}': {}".format(snippet, content_type)
            )

    else:
        if content_type == "yaml":
            try:
                yaml = YAML()
                loaded = yaml.load(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    content_origin=content_origin,
                    msg="Could not parse raw metadata as 'yaml'.",
                    reason=e,
                    solution=solution_msg(),
                )
        elif content_type == "json":
            try:
                loaded = json.loads(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    content_origin=content_origin,
                    msg="Could not parse raw metadata as 'json'.",
                    reason=e,
                    solution=solution_msg(),
                )
        elif content_type == "toml":
            try:
                loaded = toml.loads(content)
            except (Exception) as e:
                raise FrklParseException(
                    content=content,
                    content_origin=content_origin,
                    msg="Could not parse raw metadata as 'toml'.",
                    reason=e,
                    solution=solution_msg(),
                )
        else:
            raise FrklParseException(
                content=content,
                content_origin=content_origin,
                msg="Could not parse content.",
                reason=f"Unsupported data format for metadata: {content_type}",
                solution="Check format of content and specify correct content_type.",
            )

    return loaded


InputFileTypeCharacteristics = namedtuple(
    "InputTypeCharacteristics", "local git file folder"
)


class INPUT_FILE_TYPE(Enum):

    local_file = InputFileTypeCharacteristics(
        local=True, git=False, file=True, folder=False
    )
    local_dir = InputFileTypeCharacteristics(
        local=True, git=False, file=False, folder=True
    )
    git_repo = InputFileTypeCharacteristics(
        local=False, git=True, file=False, folder=True
    )
    remote_url = InputFileTypeCharacteristics(
        local=False, git=False, file=True, folder=False
    )


def determine_input_file_type(input_obj: Union[str, Path]) -> Optional[INPUT_FILE_TYPE]:

    if isinstance(input_obj, (str, Path)):

        if isinstance(input_obj, str):
            path = Path(input_obj)
        else:
            path = input_obj

        if path.exists():
            if path.is_file() or path.is_symlink() or path.is_fifo():
                return INPUT_FILE_TYPE.local_file
            elif path.is_dir():
                return INPUT_FILE_TYPE.local_dir

        if isinstance(input_obj, str) and is_url_or_abbrev(input_obj):

            url = expand_git_url(input_obj)
            if is_git_repo_url(url):
                return INPUT_FILE_TYPE.git_repo
            return INPUT_FILE_TYPE.remote_url

    return None


def determine_input_type(input_obj: Any) -> Optional[str]:

    ft = determine_input_file_type(input_obj)
    if ft is not None:
        return ft.name

    if isinstance(input_obj, str):
        return "string"
    elif isinstance(input_obj, Mapping):
        return "dict"
    elif isinstance(input_obj, Sequence):
        return "list"

    return None


def auto_convert_string(value):
    """Utility to try to guess the type of a value, and return it as that type."""

    if value.lower() in ["true", "yes"]:
        value = True
    elif value.lower() in ["false", "no"]:
        value = False
    else:
        try:
            value = int(value)
        except (Exception):
            # fine, we'll just use the string
            # TODO: support lists
            pass

    return value


def get_content(
    input_obj: Any,
    input_type: str = None,
    content_type: str = "auto",
    dict_string_separator: str = None,
    value_key=None,
    type_key="type",
    metadata_key="metadata",
) -> Any:
    """Auto determine content of the given input object.

    Automatically reads/downloads content in case the input object is a path or url string. If 'value_key' is not provided,
    the return value is the parsed content of the input object (converted to a dict or list if possible). If provided,
    the result is a dict that includes metadata about the input object and parsing process.

    Args:
        - *input_obj*: the input object
        - *input_type*: the input type
        - *content_type*: the type of the content (e.g. 'yaml', will be auto determined if not provided) - only applies to input types that have content, like a file
        - *dict_string_separator*: if not None, this method will try to convert a string to a single-key dict using the provided separator
        - *value_key*: if set, return a dict using this key for the content value
        - *type_key*: if 'value_key' is set, use this key to describe the content type
        - *metadata_key*: if 'value_key' is set, use this key for the content and parse process metadata

    Returns:
        the content as a dict (if possible), or string, optionally wrapped in a dict containing metadata about the content and parse process
    """

    if input_type is None:
        input_type = determine_input_type(input_obj)

    value = None
    metadata: Dict[str, Any] = {}
    if input_type is None:
        value = input_obj
        value_type = "unknown"
    elif input_type == "string":
        if dict_string_separator:
            if dict_string_separator in input_obj:
                key, v = input_type.split(dict_string_separator, maxsplit=2)
                v = auto_convert_string(v)
                d: Dict[str, Any] = {}
                if "." in key:
                    dpath.util.new(d, key, v, separator=".")
                else:
                    d[key] = v
                value = d
                value_type = "dict"
            else:
                value = input_obj
                value_type = "string"
                # raise Exception(f"Input string can't be converted to dict with separator '{dict_string_separator}'.")
        else:
            value = input_obj
            value_type = "string"
    elif input_type == "dict":
        value = input_obj
        value_type = "dict"
    elif input_type == "list":
        value = input_obj
        value_type = "list"
    elif input_type == "local_file":
        content = Path(input_obj).read_text()
        try:
            value, value_type = parse_file_content(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = "string"
            metadata["parse_successful"] = False
    elif input_type == "local_dir":
        raise Exception("Can't get content from local directory.")
    elif input_type == "git_repo":
        raise Exception("Can't get content from git repository.")
    elif input_type == "remote_url":
        r_url: str = expand_git_url(
            input_obj
        )  # doesn't need to be git url, but just in case...
        metadata["full_url"] = r_url
        content = download_cached_text_file(r_url, return_content=True)  # type: ignore
        try:
            value, value_type = parse_file_content(content, content_type=content_type)
            metadata["parse_successful"] = True
        except (Exception):
            value = content
            value_type = "string"
            metadata["parse_successful"] = False

    if not value_key:
        return value

    result = {}
    result[value_key] = value

    if type_key is not None:
        result[type_key] = value_type

    if metadata_key is not None:
        result[metadata_key] = metadata

    return result


def parse_file_content(content: str, content_type="auto") -> Tuple[Any, str]:

    if content_type == "text":
        return content, "string"

    parsed = auto_parse_dict_string(content, content_type=content_type)

    if isinstance(parsed, Sequence):
        value_type = "list"
    elif isinstance(parsed, Mapping):
        value_type = "dict"
    else:
        value_type = "unknown"

    return parsed, value_type


class SmartInput(object):
    def __init__(
        self,
        input_value=None,
        input_type: str = None,
        content_type="auto",
        force_content: str = None,
    ) -> None:

        self._input_value = input_value
        self._forced_input_type = input_type
        self._content_type = content_type
        self._input_type = None
        self._content = None

        self._force_content = force_content
        if self._force_content:
            self.content()

    @property
    def input_value(self):

        return self._input_value

    @input_value.setter
    def input_value(self, input_value):

        self._input_type = None
        self._content = None
        self._intput_value = input_value

    @property
    def input_type(self):

        if self._forced_input_type is not None:
            return self._forced_input_type
        if self._input_type is None:
            self._input_type = determine_input_type(self._input_value)

        return self._input_type

    @property
    def content_type(self):

        if self._content() is None:
            self.content()

        return self._content["type"]

    async def content_async(self):

        if self._content is None:
            from frtls.formats.ainput import get_content_async

            self._content = await get_content_async(
                input_obj=self.input_value,
                input_type=self.input_type,
                content_type=self._content_type,
                value_key="value",
            )
            if self._force_content:
                if not self._force_content == self._content["type"]:
                    raise TypeError(
                        f"Invalid type for smart input: {self._content['type']} (wanted: {self._force_content}"
                    )

        return self._content["value"]

    def content(self):

        if self._content is None:
            self._content = get_content(
                input_obj=self.input_value,
                input_type=self.input_type,
                content_type=self._content_type,
                value_key="value",
            )
            if self._force_content:
                if not self._force_content == self._content["type"]:
                    raise TypeError(
                        f"Invalid type for smart input: {self._content['type']} (wanted: {self._force_content}"
                    )

        return self._content["value"]

    def __repr__(self):

        resolved = False
        msg = ""
        if self._content is not None:
            resolved = True
            msg = f" input_type={self.input_type}, content_type={self.content_type}"

        return f"[SmartInput: input={self._input_value}, parsed={resolved}{msg}]"


class SmartInputChain(object):
    def __init__(self, *inputs):

        self._inputs = inputs
        self._dict_chain = []
        for i in self._inputs:
            si = SmartInput(input_value=i, force_content="dict")
            self._dict_chain.append(si)

        self._value = None

    def value(self):

        if self._value is not None:
            return self._value

        self._value = {}
        for si in self._dict_chain:
            dpath.merge(self._value, si.content())

        return self._value
