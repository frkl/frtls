# -*- coding: utf-8 -*-
from typing import Any, Mapping

from frtls.doc.explanation import Explanation
from rich.console import Console, ConsoleOptions, RenderResult


class StepsExplanation(Explanation):
    def __init__(self, data: Mapping[str, Any]):

        super().__init__(data=data)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        return self.data

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:
        result = []

        result.append("\n[bold]Steps[/bold]:")

        result.append("")
        for v in self.data.values():
            result.append(f"  - [italic]{v}[/italic]")

        return result
