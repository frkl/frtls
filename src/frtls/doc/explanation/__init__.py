# -*- coding: utf-8 -*-
import collections
from abc import ABCMeta, abstractmethod
from typing import Any, Mapping, Optional

import dpath.util
from frtls.async_helpers import wrap_async_task
from frtls.formats.output_formats import serialize
from frtls.strings import reindent as rind
from frtls.types.utils import is_instance_or_subclass
from rich.console import Console, ConsoleOptions, RenderResult


def make_serializable(value: Any) -> Any:

    result: Any = None
    if value is None:
        result = "-- no value --"
    elif isinstance(value, str):
        result = value
    elif isinstance(value, bool):
        result = "true" if value else "false"
    elif isinstance(value, (int, float)):
        result = str(value)
    elif is_instance_or_subclass(value, Explanation):
        result = make_serializable(value.explanation_data)
    elif hasattr(value, "explain"):
        try:
            temp = value.explain()
            if is_instance_or_subclass(temp, Explanation):
                result = temp
        except Exception:
            pass

    if result is None:
        if isinstance(value, collections.abc.Mapping):
            result = {}
            for k, v in value.items():
                r_k = make_serializable(k)
                r_v = make_serializable(v)
                result[r_k] = r_v
        elif isinstance(value, collections.abc.Iterable):
            result = []
            for r in value:
                result.append(make_serializable(r))
        elif hasattr(value, "to_dict"):
            result = value.to_dict()  # type: ignore
        else:
            result = str(value)

    return result


def to_value_string(value: Any, reindent: int = 0) -> str:

    r = make_serializable(value)

    if isinstance(r, str):
        r_str = r
    elif isinstance(r, (collections.abc.Sequence, collections.abc.Mapping)):
        r_str = serialize(r, format="yaml")
    else:
        r_str = str(r)

    if reindent:
        r_str = rind(r_str, reindent)

    return r_str


class Explanation(metaclass=ABCMeta):
    def __init__(self, data: Optional[Any] = None, **kwargs):

        self._data: Optional[Any] = data
        self._config: Mapping[str, Any] = kwargs

        self._explanation_data: Optional[Mapping[str, Any]] = None

    @property
    def data(self):

        return self._data

    @data.setter
    def data(self, data: Any):

        self._data = data
        self._explanation_data = None

    async def _load(self) -> None:

        expl_old = self._explanation_data
        if expl_old:
            raise NotImplementedError()

        explanation_data = await self.create_explanation_data()

        self._explanation_data = make_serializable(explanation_data)

    @property
    def explanation_data(self) -> Mapping[str, Any]:

        if self._explanation_data is None:
            wrap_async_task(self._load)
        return self._explanation_data  # type: ignore

    def explain(self, key_path: str, default: Any = None) -> Any:

        try:
            value: Any = dpath.util.get(self.explanation_data, key_path, separator=".")
            return value
        except KeyError:
            return default
        return None

    @abstractmethod
    async def create_explanation_data(self) -> Mapping[str, Any]:
        pass

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        yield to_value_string(self.explanation_data)


class SimpleExplanation(Explanation):
    def __init__(self, data: Any, data_name: "str" = "data", **kwargs):

        self._data_name: str = data_name
        super().__init__(data, **kwargs)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        return {self._data_name: self._data}


# class DictOfDictsExplanation(Explanation):
#     def __init__(self, dict_data: Mapping[str, Mapping[str, Any]]):
#
#         super().__init__(data=dict_data)
#
#     def __rich_console__(
#         self, console: Console, options: ConsoleOptions
#     ) -> RenderResult:
#
#         keys: Set[str] = set()
#
#         dict_data: Mapping[str, Mapping[str, Any]] = self.data
#
#         for k, v in dict_data.items():
#             keys.update(v.keys())
#
#         table = Table()
#         table.add_row("Name")
#         for k in keys:
#             table.add_column(k)
#
#         for k, v in dict_data.items():
#             for _v in v.values():
#                 _v_str = to_value_string(_v)
#                 table.add_row(_v_str)
#
#         yield table
