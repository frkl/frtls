# -*- coding: utf-8 -*-
from typing import Any, Hashable, Mapping

from frtls.doc.explanation import to_value_string
from frtls.introspection.pkg_env import AppEnvironment
from rich import box
from rich.console import Console
from rich.table import Table
from sortedcontainers import SortedDict


def to_key_value_table(
    data: Mapping[Hashable, Any],
    show_headers: bool = True,
    key_name: str = "Name",
    value_name: str = "Value",
    console: Console = None,
    sort: bool = False,
) -> Table:

    if console is None:
        console = AppEnvironment().get_global("console")
        if console is None:
            # TODO: make default styles
            console = Console()

    table = Table(show_header=show_headers, box=box.SIMPLE)
    table.add_column(key_name, style="key2")
    table.add_column(value_name, style="value")

    if sort:
        data = SortedDict(data)

    for k, v in data.items():
        _k = to_value_string(k)
        _v = to_value_string(v)
        table.add_row(_k, _v)

    return table
