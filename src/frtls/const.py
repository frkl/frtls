# -*- coding: utf-8 -*-
from enum import Enum


class OutputFormat(Enum):

    YAML = "yaml"
    RAW = "raw"
    JSON = "json"
    JSON_LINE = "json-line"
    PFORMAT = "pformat"
