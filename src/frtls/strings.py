# -*- coding: utf-8 -*-
import logging
import re
from typing import Any, Dict, Iterable, List, Mapping, Optional

from frtls.defaults import DEFAULT_URL_ABBREVIATIONS_FILE
from validators import url as validate_url  # type: ignore


log = logging.getLogger("frtls")

CAMEL_CALSE_FIRST = re.compile("(.)([A-Z][a-z]+)")
CAMEL_CALSE_ALL = re.compile("([a-z0-9])([A-Z])")


def from_camel_case(text: str, sep: str = "_", lower: bool = True):
    """Convert a string from camel case.

    Args:
        - *text*: the string
        - *sep*: the seperator to use
        - *lower*: whether to convert the result to all lowercase
    """

    text = CAMEL_CALSE_FIRST.sub(fr"\1{sep}\2", text)
    text = CAMEL_CALSE_ALL.sub(fr"\1{sep}\2", text)
    if lower:
        text = text.lower()
    return text


def reindent(s: str, numSpaces: int, keep_current: bool = True, line_nrs: bool = False):
    """Reindents a string.

    Args:
        - *s*: the string
        - *numSpaces*: the indent
        - *keep_current*: keep a potential current indention and add to it
        - *line_nrs*: whether to display line numbers
    Returns:
        - the indented string
    """

    tokens: List[str] = s.split("\n")
    if keep_current:
        tokens = [(numSpaces * " ") + line for line in tokens]
    else:
        tokens = [(numSpaces * " ") + line.lstrip() for line in tokens]

    if line_nrs is not False:
        if isinstance(line_nrs, int):
            begin = line_nrs
        else:
            begin = 0

        temp = []
        for index, line in enumerate(tokens):
            temp.append("{:3d} {}".format(index + begin, line))

        tokens = temp

    return "\n".join(tokens)


def expand_git_url(
    url: str, abbrevs: Dict[str, str] = DEFAULT_URL_ABBREVIATIONS_FILE
) -> str:
    """Expands an url using (optionally provided) template values in 'repl_dict'.
    """

    template: Optional[str] = None
    abbrev: str
    for key in abbrevs.keys():

        if not url.startswith(key):
            continue

        template = abbrevs[key]
        abbrev = key
        break

    if template is None:
        return url

    tokens = url[len(abbrev) + 1 :].split("/", maxsplit=3)  # noqa  # type: ignore

    if len(tokens) < 2:
        raise ValueError(
            f"Abbreviated url too short, needs at least user- and repo-name: {url}"
        )

    user = tokens[0]
    repo = tokens[1]

    if "::" in repo:
        if not repo.endswith("::"):
            raise ValueError(
                f"Invalid abbreviation format (if specifying branch, string must end with '::'): {url}"
            )
        repo, branch = repo[0:-2].split("::")
    else:
        branch = "master"

    if len(tokens) == 3:
        result = template.format(user=user, repo=repo, branch=branch, path=tokens[2])
    else:
        result = template.format(user=user, repo=repo, branch=branch)

    return result


def is_url_or_abbrev(url: str, abbrevs: Dict = DEFAULT_URL_ABBREVIATIONS_FILE):
    """Check if the suplied string is an url or url abbreviation.

    Args:
        - *url*: the string to be tested
        - *abbrevs*: an url-abbreviation dictionary
    """

    for key in abbrevs.keys():
        if url.startswith(f"{key}:"):
            url = expand_git_url(url, abbrevs=abbrevs)

    return validate_url(url)


def is_git_repo_url(url):

    # from: https://github.com/jonschlinkert/is-git-url/
    regex = "([git|ssh|https]|git@[-\w.]+):(\/\/)?(.*?)\.git$"  # noqa
    return re.search(regex, url)


def find_free_name(
    stem: str,
    current_names: Iterable[str],
    method="count",
    method_args: Mapping[str, Any] = None,
    sep="_",
):

    if method != "count":
        raise NotImplementedError()

    if method_args is None:
        try_no_postfix = True
        start_count = 1
    else:
        try_no_postfix = method_args.get("try_no_postfix", True)
        start_count = method_args.get("start_count", 1)

    if try_no_postfix:
        if stem not in current_names:
            return stem

    i = start_count

    # new_name = None
    while True:
        new_name = f"{stem}{sep}{i}"
        if new_name in current_names:
            i = i + 1
            continue
        break
    return new_name
